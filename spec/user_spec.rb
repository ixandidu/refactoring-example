require 'support/overrides/stripe'
require 'support/overrides/exceptions'

RSpec.describe User, type: :model do
  it { is_expected.to have_many(:devices).dependent(:destroy) }

  context 'validations' do
    it { is_expected.to allow_value(nil).for(:password) }
    it do
      is_expected.to validate_length_of(:password)
                     .is_at_least(9)
                     .with_message('is invalid')
    end

    it { is_expected.to validate_presence_of :name }

    context 'email regex validation' do
      %w[
        ixandidu@gmail.com
        i@gmail.com
        ixandidu@mail.google.com
        ixandidu@mail.google.ca
      ].each do |valid_email|
        it { is_expected.to allow_value(valid_email).for(:email) }
      end

      %w[
        @@gmail.com
        ixandidu@.com
        ixandidu@gmail
        ixandidu@gmail
        ixandidu@gmail.c
      ].each do |invalid_email|
        it { is_expected.not_to allow_value(invalid_email).for(:email) }
      end
    end

    it do
      is_expected.to validate_numericality_of(:device_limit)
                     .is_greater_than(-1)
                     .with_message('Device limit count is invalid')
    end

    context 'when `device_limit` valid' do
      before do
        allow(subject.errors).to receive(:key?).with(:device_limit) { false }
      end

      it do
        is_expected.to validate_numericality_of(:event_limit)
                       .is_greater_than(-1)
                       .with_message('Event limit count is invalid')
      end
    end

    context 'when `device_limit` and `event_limit` valid' do
      before { allow(subject.errors).to receive(:keys) { [] } }

      it do
        is_expected.to validate_inclusion_of(:discount)
                       .in_range(0..100)
                       .with_message('Discount is invalid')
      end
    end
  end

  describe '#update_card' do
    let(:user) { User.new(email: 'ixandidu@gmail.com') }
    let(:stripe_customer) do
      double(
        Stripe::Customer,
        :cards         => stripe_card_class,
        :[]            => 'fake-customer-id',
        :default_card= => true,
        :save          => true
      )
    end
    let(:stripe_card_class) do
      double(Stripe::Card, create: stripe_card_instance)
    end
    let(:stripe_card_instance) do
      double(Stripe::Card, id: 'fake-card-id', :[] => 'fake-last4')
    end

    RSpec.shared_examples 'updating card by token' do
      context 'after we have an instance of `Stripe::Customer`', order: :defined do
        it 'create new customer cards' do
          expect(stripe_card_class).to have_received(:create).with(
            card: 'fake-token'
          )
        end

        it 'set the card as default' do
          expect(stripe_customer).to have_received(:default_card=).with(
            stripe_card_instance.id
          )
          expect(stripe_customer).to have_received(:save)
        end

        it 'save the last 4 digit of the credit card' do
          expect(user).to have_received(:last4=).with('fake-last4')
          expect(user).to have_received(:save).at_least(1).times
        end
      end
    end

    context 'when `#stripe_id` is nil', order: :defined do
      before do
        allow(user).to receive_messages(
          :id         => 'fake-user-id',
          :save       => true,
          :last4=     => true,
          :stripe_id= => true
        )
        allow(Stripe::Customer).to receive(:create) { stripe_customer }

        user.update_card('fake-token')
      end

      it 'create new Stripe::Customer' do
        expect(Stripe::Customer).to have_received(:create).with(
          email:       user.email,
          description: "ID #{user.id}"
        )
      end

      it 'set the user `stripe_id`' do
        expect(user).to have_received(:stripe_id=).with(stripe_customer['id'])
      end

      include_examples 'updating card by token'
    end

    context 'when `#stripe_id` present' do
      before do
        allow(user).to receive_messages(
          :save      => true,
          :last4=    => true,
          :stripe_id => 'fake-customer-id'
        )
        allow(Stripe::Customer).to receive(:retrieve) { stripe_customer }

        user.update_card('fake-token')
      end

      it 'retrieve existing Stripe::Customer by #stripe_id' do
        expect(Stripe::Customer).to have_received(:retrieve).with(user.stripe_id)
      end

      include_examples 'updating card by token'
    end
  end

  describe '#add_new_device' do
    context 'when the number of user devices has reach the limit' do
      let(:user) { User.new(device_limit: 1) }
      before     { allow(user).to receive(:devices) { [Device.new] } }

      it 'raise Exceptions::DeviceLimitReached' do
        expect { user.add_new_device(anything) }.to raise_error(
          Exceptions::DeviceLimitReached
        )
      end
    end

    context 'when the number of user devices has not reach the limit' do
      let(:user) do
        User.create!(
          name:         'Ikhsan Maulana',
          email:        'ixandidu@gmail.com',
          device_limit: 1,
          event_limit:  1,
          discount:     50
        )
      end

      context 'given invalid device attributes' do
        it do
          expect { user.add_new_device({}) }.to raise_error(
            ActiveRecord::RecordInvalid
          )
        end
      end

      context 'given valid device attributes' do
        let(:device_attrs) do
          {
            device_type: 'smartphone',
            name:        'Android Smartphone',
            os:          'android'
          }
        end

        before { allow(user).to receive(:touch).and_call_original }

        context 'when ActiveRecord::StaleObjectError raise' do
          before do
            allow(user).to         receive(:reload).and_call_original
            allow(user.devices).to receive(:create!).with(device_attrs).and_raise(
                                     ActiveRecord::StaleObjectError.new(
                                       user,
                                       :create
                                     )
                                   )

            user.add_new_device(device_attrs)
          end

          it { expect(user).to have_received(:reload).with(no_args).exactly(1) }
          it { expect(user).to have_received(:touch).with(no_args).exactly(2) }
        end

        it 'force update its lock version' do
          expect { user.add_new_device(device_attrs) }.to change {
            user.updated_at
          }
        end

        it 'add the device' do
          expect { user.add_new_device(device_attrs) }.to change {
            user.devices.count
          }.by(1)
        end
      end
    end
  end
end
