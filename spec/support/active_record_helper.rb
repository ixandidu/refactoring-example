require 'active_record'
require 'device'
require 'user'

ActiveRecord::Base.establish_connection(
  adapter:  'sqlite3',
  database: ':memory:'
)

ActiveRecord::Schema.define do
  self.verbose = false

  create_table :devices, force: true do |t|
    t.integer :user_id
    t.string  :token
    t.string  :device_type
    t.string  :name
    t.string  :os

    t.timestamps null: false
  end

  create_table :users, force: true do |t|
    t.string  :name
    t.string  :email
    t.string  :crypted_password
    t.string  :salt
    t.boolean :admin,           default: false
    t.string  :stripe_id
    t.string  :last4
    t.integer :device_limit
    t.integer :event_limit
    t.integer :discount

    t.timestamps null: false
  end
end
