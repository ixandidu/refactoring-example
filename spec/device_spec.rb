RSpec.describe Device, type: :model do
  it { is_expected.to belong_to :user }

  context 'generate unique token on create' do
    let(:device) do
      Device.create(
        device_type: 'smartphone',
        name:        'android smartphone',
        os:          'android'
      )
    end

    it { expect(device.token).to be_present }
  end

  %i[device_type name os].each do |attr|
    it { is_expected.to validate_presence_of attr }
  end
end
