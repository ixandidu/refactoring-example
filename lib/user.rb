# == Schema Information
#
# Table name: users
#
#  id                              :integer          not null, primary key
#  email                           :string(255)      not null
#  crypted_password                :string(255)      not null
#  salt                            :string(255)      not null
#  created_at                      :datetime
#  updated_at                      :datetime
#  admin                           :boolean          default(FALSE)
#  stripe_id                       :string(255)
#  last4                           :string(255)
#  name                            :string(255)
#

class User < ActiveRecord::Base
  has_many :devices, dependent: :destroy

  attr_accessor :password

  validates :password,
            length: {minimum: 9, message: 'is invalid'},
            allow_nil: true
  validates :name, presence: true
  validates :email,
            format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  validates :device_limit,
            numericality: {
              greater_than: -1,
              message:      'Device limit count is invalid'
            }
  validates :event_limit,
            numericality: {
              greater_than: -1,
              message:      'Event limit count is invalid',
              unless:       'errors.key?(:device_limit)'
            }
  validates :discount,
            inclusion: {
              in:      0..100,
              message: 'Discount is invalid',
              unless:  '(errors.keys & [:device_limit, :event_limit]).any?'
            }

  def update_card(token)
    new_credit_card              = stripe_customer.cards.create(card: token)
    stripe_customer.default_card = new_credit_card.id
    stripe_customer.save

    self.last4 = new_credit_card[:last4]
    save
  end

  def add_new_device(device_attrs)
    User.transaction do
      begin
        if devices.count >= device_limit
          raise Exceptions::DeviceLimitReached.new("device_limit_reached")
        end
        # Force parent object to update its lock version as child object
        # creation in has_many association skips locking mechanism.
        touch
        devices.create!(device_attrs)
      rescue ActiveRecord::StaleObjectError
        reload and retry
      end
    end
  end

  private
    def stripe_customer
      @stripe_customer ||= if stripe_id
        Stripe::Customer.retrieve(stripe_id)
      else
        Stripe::Customer.create(
          email:       email,
          description: "ID #{id}"
        ).tap do |new_customer|
          self.stripe_id = new_customer['id']
        end
      end
    end
end
